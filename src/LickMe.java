import javax.crypto.Cipher;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


/**
 * @author Bruno Núñez Manuel   1ºDAM DUAL B
 * @since 25/05/2021
 * @version 1.0
 */
public class LickMe {
    public static void main(String[] args) {
        // Duda: un unreferenced InputStream, necesita close() o ya se encarga el Garbage Collector?
        // Por ejemplo: byte[] clavePublicaBytes = new FileInputStream(file).readAllBytes();
        try {
            String algoritmo = "RSA";
            PublicKey publicKey = getPublicKeyFromFile(new File("src/clavePublica"), algoritmo);
            PrivateKey privateKey = getPrivateKeyFromFile(new File("src/clavePrivadaSOLOCOMPROBAR"),algoritmo);
            Cipher cipher = Cipher.getInstance(algoritmo);

            /*a) Sea capaz de leer la firma digital (instruccionesFirma) y mediante ella verificar la autenticidad
            o no de los tres ficheros de instrucciones que se adjuntan. (3 puntos)*/
            Signature signature = Signature.getInstance("MD5withRSA");
            signature.initVerify(publicKey);
            signature.update(new FileInputStream("src/instrucciones2.txt").readAllBytes()); // El bueno.
            byte[] firmaLeidaBytes = new FileInputStream("src/instruccionesFirma").readAllBytes();
            if (signature.verify(firmaLeidaBytes)) {
                System.out.println("El mensaje es auténtico.");
            } else {
                System.out.println("El mensaje NO es auténtico.");
            }

            /*b) Sea capaz de leer la clave pública (clavePublica) y, con ella, cifrar un fichero de texto que contenga
            vuestra filtración. Vuestra filtración deberá estar en un fichero de texto siguiendo el formato que se
            detalla en las instrucciones auténticas. Tras cifrar, creará un fichero con la filtración cifrada que
            también tenéis que entregar. (4 puntos)*/
            byte[] filtracionBytes = Files.readAllBytes(Path.of("src/mi_filtración.txt"));;
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encriptedMessageBytes = cipher.doFinal(filtracionBytes);
            FileOutputStream fileOutputStream = new FileOutputStream("src/filtracionCifrada");
            fileOutputStream.write(encriptedMessageBytes);
            fileOutputStream.close();

            /*c) Sea capaz de leer la clavePrivada (clavePrivadaSOLOCOMPROBAR) y descifrar el mensaje que habéis
            cifrado en el punto anterior. Esto será solo para que podáis verificar que todoh ha funcionado bien
            (2 puntos).*/
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decriptedMessageBytes =
                    cipher.doFinal(new FileInputStream("src/filtracionCifrada").readAllBytes());
            FileOutputStream fileOutputStream2 = new FileOutputStream("src/filtracionDESCifrada");
            fileOutputStream2.write(decriptedMessageBytes);
            fileOutputStream2.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param file continente de la clave pública.
     * @param algoritmo algoritmo que usa la clave privada.
     * @return la clave pública del archvivo.
     * @throws Exception
     */
    static public PublicKey getPublicKeyFromFile(File file, String algoritmo) throws Exception {
        byte[] clavePublicaBytes = new FileInputStream(file).readAllBytes();
        X509EncodedKeySpec spec = new X509EncodedKeySpec(clavePublicaBytes);
        KeyFactory factory = KeyFactory.getInstance(algoritmo);
        return factory.generatePublic(spec);
    }

    /**
     * @param file continente de la clave privada.
     * @param algoritmo algoritmo que usa la clave privada.
     * @return la clave privada del archivo.
     * @throws Exception
     */
    static public PrivateKey getPrivateKeyFromFile(File file, String algoritmo) throws Exception {
        byte[] clavePrivadaBytes = new FileInputStream(file).readAllBytes();
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(clavePrivadaBytes);
        KeyFactory factory = KeyFactory.getInstance(algoritmo);
        return factory.generatePrivate(spec);
    }

}
